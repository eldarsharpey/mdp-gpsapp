//
//  LoadingScreenViewController.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 19.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingScreenViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *label;

@property (weak, nonatomic) IBOutlet UIProgressView *pBar;
@end
