//
//  AppDelegate.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 17.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CLLocationManager* locationManager;
@property (strong, nonatomic) CLGeocoder* geocoder;
@property (strong, nonatomic) CLPlacemark* placemark;
@property (readonly, strong) NSPersistentContainer *persistentContainer;



@property (strong, nonatomic) NSArray* TodayData;
@property (strong, nonatomic) NSArray* placesArray;
@property (strong, nonatomic)  NSString* placeString;

@property (assign, nonatomic) BOOL GPSloaded;

- (void)saveContext;
-(void)AddDataToday;
-(void) CountPoints;
-(void)RefreshTodayPlacesMarks;
-(NSArray* ) getBarData;

@end

