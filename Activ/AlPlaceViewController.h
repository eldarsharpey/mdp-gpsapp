//
//  AlPlaceViewController.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 18.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlPlaceViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSDictionary* markDictionary;
@property (assign, nonatomic)  NSUInteger rowNum;
@property (weak, nonatomic) IBOutlet UITableView *allPlaceTab;

@property(strong, nonatomic) NSArray* places;

@end
