//
//  StoryDataViewController.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 19.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import "StoryDataViewController.h"
#import "AppDelegate.h"
#import "DataToday+CoreDataProperties.h"
#import "PlacesToday+CoreDataProperties.h"
#import "FullStoryViewController.h"

@interface StoryDataViewController ()

@end

@implementation StoryDataViewController

NSArray* todayData;
NSDictionary* pointsRangeImg;
NSDictionary* pointsRangeText;
NSInteger rowNum;

-(void)viewWillAppear:(BOOL)animated{
    todayData = [(AppDelegate*)[[UIApplication sharedApplication] delegate] TodayData];
    
    pointsRangeImg = @{@"0" :@"😩", @"1" :@"😔", @"2":@"🤓", @"3":@"😎", @"4":@"🤪",@"4":@"🤩"};
    pointsRangeText = @{@"0" :@"Нет активности", @"1" :@"Пошевелил животиком", @"2":@"Помог маме с пакетами", @"3":@"Почистил 10кг картошки", @"4":@"Поднял 100кг штангу",@"5":@"Активнее тебя только фтор!"};
    
     [_tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger count = [todayData count];
    return count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *c;
    
    c = [tableView dequeueReusableCellWithIdentifier:@"TodayDataCell"];
    
    UILabel *date;
    UILabel *mark;
    UILabel *img;
    NSInteger index = [indexPath row];
    
    
   DataToday * todayDataObj = todayData[index];
    
    date = (UILabel*)[c.contentView viewWithTag:1001];
    mark = (UILabel*)[c.contentView viewWithTag:1002];
    img = (UILabel*)[c.contentView viewWithTag:1003];
    
    date.text = todayDataObj.date;
    NSString * markstring =@"Метка: ";
    mark.text = [markstring stringByAppendingString:[pointsRangeText valueForKey:[NSString stringWithFormat:@"%d",todayDataObj.points]]];
    img.text = [pointsRangeImg valueForKey:[NSString stringWithFormat:@"%d",todayDataObj.points]];
    
    return c;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    rowNum = indexPath.row;
    
    [self performSegueWithIdentifier:@"storySegue" sender:self];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"storySegue"]){
        FullStoryViewController* story = segue.destinationViewController;
        story.numOfRow= rowNum;
    }
    
}

@end
