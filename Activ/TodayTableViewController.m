//
//  TodayTableViewController.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 18.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import "TodayTableViewController.h"
#import "LoadingScreenViewController.h"
#import "AppDelegate.h"
#import "DataToday+CoreDataProperties.h"
#import "PlacesToday+CoreDataProperties.h"

@interface TodayTableViewController ()

@end

@implementation TodayTableViewController

NSArray* todayArray;
NSDictionary* markDictionary;
NSDictionary* pointsDictionary;

NSDictionary* pointsRangeImg1;
NSDictionary* pointsRangeText1;

-(void)viewWillAppear:(BOOL)animated{
    if(_GPSloaded){
        todayArray = [(AppDelegate*)[[UIApplication sharedApplication] delegate] TodayData];
        [_todayTableView reloadData];
        
        pointsRangeImg1 = @{@"0" :@"😩", @"1" :@"😔", @"2":@"🤓", @"3":@"😎", @"4":@"🤪",@"5":@"🤩"};
        pointsRangeText1 = @{@"0" :@"Нет активности", @"1" :@"Пошевелил животиком", @"2":@"Помог маме с пакетами", @"3":@"Почистил 10кг картошки", @"4":@"Поднял 100кг штангу",@"5":@"Активнее тебя только фтор!"};
        
        
        
        DataToday* todayDataObj = [todayArray lastObject];
        _date.text =[todayDataObj.date substringWithRange:NSMakeRange(0, 2)];
        _month.text =[todayDataObj.date substringWithRange:NSMakeRange(3, 4)];
        _year.text = [todayDataObj.date substringWithRange:NSMakeRange(9, 5)];
        _imgPoints.text = [pointsRangeImg1 valueForKey: [NSString stringWithFormat:@"%d",todayDataObj.points]];
        _discripPoints.text = [pointsRangeText1 valueForKey: [NSString stringWithFormat:@"%d",todayDataObj.points]];
    }
    
}


-(void)viewDidAppear:(BOOL)animated{
    if(_GPSloaded){

    }else{
        _GPSloaded = true;
        [self performSegueWithIdentifier:@"load" sender:self];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    markDictionary = @{@"Нет":@"👀", @"Дом":@"🏰", @"Спорт":@"🏋️‍♀️", @"Учеба":@"🤯", @"Работа":@"🏦"};
    pointsDictionary = @{@"Нет":@"0️⃣", @"Дом":@"1️⃣", @"Спорт":@"4️⃣", @"Учеба":@"3️⃣", @"Работа":@"2️⃣"};
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    DataToday* todayDataObj = [todayArray lastObject];
    NSInteger count = [[todayDataObj placesToday] count];
    return count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    UITableViewCell *c;
    
    c = [tableView dequeueReusableCellWithIdentifier:@"TodayCell"];
    
    UILabel *place;
    UILabel *mark;
    UILabel *img;
    UILabel *points;
    NSInteger index = [indexPath row];
    
    
    DataToday* todayDataObj = [todayArray lastObject];
    
    PlacesToday* placesTodayObj = [[todayDataObj placesToday] objectAtIndex:index];
    
    place = (UILabel*)[c.contentView viewWithTag:1001];
    mark = (UILabel*)[c.contentView viewWithTag:1002];
    img = (UILabel*)[c.contentView viewWithTag:1003];
    points = (UILabel*)[c.contentView viewWithTag:1004];
    
    place.text = placesTodayObj.place;
    NSString * markstring =@"Метка: ";
    mark.text = [markstring stringByAppendingString:placesTodayObj.mark];
    img.text = [markDictionary  valueForKey:placesTodayObj.mark];
    points.text = [pointsDictionary valueForKey: placesTodayObj.mark];
    return c;
     
}



@end
