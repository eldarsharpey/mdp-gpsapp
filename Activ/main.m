//
//  main.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 17.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
