//
//  AppDelegate.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 17.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import "AppDelegate.h"
#import "Places+CoreDataProperties.h"
#import "DataToday+CoreDataProperties.h"
#import "PlacesToday+CoreDataProperties.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


NSDictionary* pointsRate;


NSManagedObjectContext *context;

-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
     _GPSloaded = false;
    
    pointsRate = @{@"Нет":@0, @"Дом":@1, @"Спорт":@4, @"Учеба":@3, @"Работа":@2};
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
     context = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    [self CreateLocationManager];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    [self saveContext];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self saveContext];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - CheckLocation





-(void)AddNewPlaceToAllPlace{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* description = [NSEntityDescription entityForName:@"Places" inManagedObjectContext:context];
    [request setEntity:description];
    [request setResultType: NSManagedObjectResultType];
    
    NSArray* result = [context executeFetchRequest:request error:nil];
    
    
    bool finded = false;
    
    if([result count] == 0){
        Places* place = [NSEntityDescription insertNewObjectForEntityForName:@"Places" inManagedObjectContext:context];
        place.place = _placeString;
        place.mark = @"Нет";
        [self saveContext];
        NSLog(@"AllPlaces result == NULL, added new place");
    }else{
        for (Places* placeObj in result) {
            if([placeObj.place isEqualToString:_placeString]){
                finded = true;
                NSLog(@"AllPlace Alredy Exist");
            }
        }
        
        if(!finded){
            Places* place = [NSEntityDescription insertNewObjectForEntityForName:@"Places" inManagedObjectContext:context];
            place.place = _placeString;
            place.mark = @"Нет";
            [self saveContext];
            NSLog(@"Added new ALLplace, but there ara exist");
        }
    }
    
    NSArray* AllPlacesResult = [context executeFetchRequest:request error:nil];
    
    for (Places* object in AllPlacesResult) {
        NSLog(@"%@", object.place);
        NSLog(@"%@", object.mark);
    }

    _placesArray = AllPlacesResult;
}



-(void) AddDataToday{
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* description = [NSEntityDescription entityForName:@"DataToday" inManagedObjectContext:context];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd MMMM, yyyy"];
    
    NSDate* now = [[NSDate alloc] init];
    NSString* dateString = [format stringFromDate:now];
    
    
    request = [[NSFetchRequest alloc] init];
    description = [NSEntityDescription entityForName:@"DataToday" inManagedObjectContext:context];
    [request setEntity:description];
    [request setResultType: NSManagedObjectResultType];
    
    NSArray* result= [context executeFetchRequest:request error:nil];
    
    //Сверху создаем дату и ищем в бд DataToday оьекты
    
    bool finded = false;
    
    if([result count] == 0){
        DataToday* dataToday = [NSEntityDescription insertNewObjectForEntityForName:@"DataToday" inManagedObjectContext:context];
        dataToday.date = dateString;
        dataToday.points = 0;
        dataToday.steps = 0;
        [self AddPlacesToday:dataToday];
        [self saveContext];
        NSLog(@"Today places null added new place in today places");
    }else{
        for (DataToday *dataTodayObj in result) {
            if([dataTodayObj.date isEqualToString: dateString]){
                finded = true;
                [self AddPlacesToday:dataTodayObj];
                NSLog(@"Date %@ Alredy Exist", dateString);
            }
        }
        
        if(!finded){
            DataToday* dataToday = [NSEntityDescription insertNewObjectForEntityForName:@"DataToday" inManagedObjectContext:context];
            dataToday.date = dateString;
            dataToday.points = 0;
            dataToday.steps = 0;
            [self AddPlacesToday:dataToday];
            [self saveContext];
            NSLog(@"Added new Date %@, entity not null", dateString);
        }
    }
    
    
    request = [[NSFetchRequest alloc] init];
    description = [NSEntityDescription entityForName:@"DataToday" inManagedObjectContext:context];
    [request setEntity:description];
    [request setResultType: NSManagedObjectResultType];
    
    result= [context executeFetchRequest:request error:nil];
    
    _TodayData = result;
}


-(void)AddPlacesToday:(DataToday*) dataToday{
    
    // Если сегодня мест не было то добавляем текущее место
    if ([dataToday.placesToday count] == 0) {
        PlacesToday* place = [NSEntityDescription insertNewObjectForEntityForName:@"PlacesToday" inManagedObjectContext:context];
        place.place = _placeString;
        place.mark = @"Нет";
        [dataToday addPlacesTodayObject:place];
        [self saveContext];
        NSLog(@"PlacesToday result == NULL, added new place");
    }else{
    
     bool finded = false;
    //сравнимаем каждое место которыое мы сегодня посетили с имеющимися в аллплейс и выставляем меткe
        for (PlacesToday* places in dataToday.placesToday) {
            for(Places* allPlaces in _placesArray ){
                if([places.place isEqualToString:allPlaces.place] && [places.place isEqualToString:_placeString]){
                    places.mark = allPlaces.mark;
                    finded = true;
                    [self saveContext];
                    NSLog(@"TodayPlace exist in AllPlaces equiled %@ = %@", places.mark, allPlaces.mark);
                }
            }
        }
        
        if(!finded){
            PlacesToday* place = [NSEntityDescription insertNewObjectForEntityForName:@"PlacesToday" inManagedObjectContext:context];
            place.place = _placeString;
            place.mark = @"Нет";
            [dataToday addPlacesTodayObject:place];
            [self saveContext];
            NSLog(@"PlacesToday mathces not finded but other exisr, added new place");
        }
    }
}





#pragma mark - Location

-(void) CreateLocationManager{
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager requestAlwaysAuthorization];
    
    _locationManager.delegate =self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _geocoder = [[CLGeocoder alloc]init];
    
    [_locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
    
    // Reverse Geocoding
    NSLog(@"Resolving the Address");
    [_geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            _placemark = [placemarks lastObject];
            _placeString = [NSString stringWithFormat:@"%@, %@",_placemark.thoroughfare, _placemark.locality];
            
            
            [self SaveLocation];
             _GPSloaded = true;
            
        } else {
            _placeString = @"Неопределенное место";
            [self SaveLocation];
            _GPSloaded = true;
            NSLog(@"%@", error.debugDescription);
        }
    } ];
    
    [_locationManager stopUpdatingLocation];
}



-(void) SaveLocation{
  /*
     [self AddNewPlaceToAllPlace];
     [self AddDataToday];
   
   */
     [self addRandomTestPlaces];
     [self addRandomTodayData];
/*
      [self LoadAllPlacesOnly];
      [self LoadTodayDataOnly];
    */

    
    [self CountPoints];
}


-(void) CountPoints{
     NSFetchRequest* request = [[NSFetchRequest alloc] init];
     NSEntityDescription* description = [NSEntityDescription entityForName:@"DataToday" inManagedObjectContext:context];
    [request setEntity:description];
    [request setResultType: NSManagedObjectResultType];
    NSArray* result= [context executeFetchRequest:request error:nil];
    
    for(DataToday* todayData in result){
            NSInteger score = 0;
        for(PlacesToday* places in todayData.placesToday){
            score = score + [[pointsRate valueForKey:places.mark] integerValue];
        }
        
        score = roundf(score/2);
        
        if(score>5){
            score = 5;
        }
        todayData.points = (int)score;
    }
        [self saveContext];
    }



-(NSArray* ) getBarData{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* description = [NSEntityDescription entityForName:@"DataToday" inManagedObjectContext:context];
    [request setEntity:description];
    [request setResultType: NSManagedObjectResultType];
    NSArray* result= [context executeFetchRequest:request error:nil];
    return result;
}



-(void)RefreshTodayPlacesMarks{
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* description = [NSEntityDescription entityForName:@"PlacesToday" inManagedObjectContext:context];
    [request setEntity:description];
    [request setResultType: NSManagedObjectResultType];
    NSArray* resultPlacesToday= [context executeFetchRequest:request error:nil];
    
    
    request = [[NSFetchRequest alloc] init];
    description = [NSEntityDescription entityForName:@"Places" inManagedObjectContext:context];
    [request setEntity:description];
    [request setResultType: NSManagedObjectResultType];
    NSArray* resultAllPlaces= [context executeFetchRequest:request error:nil];
    
    for (PlacesToday* places in resultPlacesToday) {
        for(Places* allPlaces in resultAllPlaces ){
            if([places.place isEqualToString:allPlaces.place]){
                places.mark = allPlaces.mark;
                [self saveContext];
                NSLog(@"Refreshing Marks equiled %@ = %@", places.mark, allPlaces.mark);
            }
        }
    }
     [self saveContext];
}


-(void)addRandomTestPlaces{
    Places* place = [NSEntityDescription insertNewObjectForEntityForName:@"Places" inManagedObjectContext:context];
    place.place = @"улица Федора Абрамова, Санкт-Петербург";
    place.mark = @"Нет";
    NSLog(@"Added %@", place.place);
    place = [NSEntityDescription insertNewObjectForEntityForName:@"Places" inManagedObjectContext:context];
    place.place = @"Верхний 3-й переулок, Санкт-Петербург";
    place.mark = @"Нет";
    NSLog(@"Added %@", place.place);
    place = [NSEntityDescription insertNewObjectForEntityForName:@"Places" inManagedObjectContext:context];
    place.place = @"Каменоостровский проспект, Санкт-Петербург";
    place.mark = @"Нет";
    NSLog(@"Added %@", place.place);
    place = [NSEntityDescription insertNewObjectForEntityForName:@"Places" inManagedObjectContext:context];
    place.place = @"улица Професора Попова, Санкт-Петербург";
    place.mark = @"Нет";
    NSLog(@"Added %@", place.place);
    [self saveContext];
    
    [self LoadAllPlacesOnly];
    
}


-(void)LoadAllPlacesOnly{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* description = [NSEntityDescription entityForName:@"Places" inManagedObjectContext:context];
    [request setEntity:description];
    [request setResultType: NSManagedObjectResultType];
    
    NSArray* result= [context executeFetchRequest:request error:nil];
    
    _placesArray = result;
}

-(void)addRandomTodayData{
    DataToday* dataToday = [NSEntityDescription insertNewObjectForEntityForName:@"DataToday" inManagedObjectContext:context];
    dataToday.date = @"21 March, 2018";
    dataToday.points = 0;
    dataToday.steps = 0;
    [dataToday addPlacesToday:[self addRandomTodayPlaces]];
    
    dataToday = [NSEntityDescription insertNewObjectForEntityForName:@"DataToday" inManagedObjectContext:context];
    dataToday.date = @"22 March, 2018";
    dataToday.points = 0;
    dataToday.steps = 0;
    [dataToday addPlacesToday:[self addRandomTodayPlaces]];
    
    dataToday = [NSEntityDescription insertNewObjectForEntityForName:@"DataToday" inManagedObjectContext:context];
    dataToday.date = @"23 March, 2018";
    dataToday.points = 0;
    dataToday.steps = 0;
    [dataToday addPlacesToday:[self addRandomTodayPlaces]];
    
    [self LoadTodayDataOnly];
    [self saveContext];
}


-(void)LoadTodayDataOnly{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* description = [NSEntityDescription entityForName:@"DataToday" inManagedObjectContext:context];
    [request setEntity:description];
    [request setResultType: NSManagedObjectResultType];
    
    NSArray* result= [context executeFetchRequest:request error:nil];
    
    _TodayData = result;
}



-(NSOrderedSet*)addRandomTodayPlaces{
    
    NSArray* array = [[NSArray alloc] init];
    
    NSArray* arr = [NSArray arrayWithObjects:@"Верхний 3-й переулок, Санкт-Петербург", @"улица Професора Попова, Санкт-Петербург",@"Каменоостровский проспект, Санкт-Петербург", nil];
    
    
    PlacesToday* place = [NSEntityDescription insertNewObjectForEntityForName:@"PlacesToday" inManagedObjectContext:context];
    place.place = [arr objectAtIndex:arc4random_uniform(3)];
    place.mark = @"Нет";
    
    array = [array arrayByAddingObject:place];
    
    place = [NSEntityDescription insertNewObjectForEntityForName:@"PlacesToday" inManagedObjectContext:context];
    place.place = [arr objectAtIndex:arc4random_uniform(3)];
    place.mark = @"Нет";
    
    array = [array arrayByAddingObject:place];
    
    place = [NSEntityDescription insertNewObjectForEntityForName:@"PlacesToday" inManagedObjectContext:context];
    place.place = [arr objectAtIndex:arc4random_uniform(3)];
    place.mark = @"Нет";
    array = [array arrayByAddingObject:place];
    
    NSOrderedSet* set = [NSOrderedSet orderedSetWithArray:array];
    
    [self saveContext];
    NSLog(@"Addedd random test today places");
    
    return set;
}



#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Activ"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}



@end
