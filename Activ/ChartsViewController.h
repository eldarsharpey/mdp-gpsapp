//
//  ChartsViewController.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 20.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBChartView.h"
#import "JBBarChartView.h"
#import "JBLineChartView.h"

@interface ChartsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *chartView;

@end
