//
//  FullStoryViewController.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 20.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullStoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *todayTableView;
@property (weak, nonatomic) IBOutlet UILabel *imgPoints;
@property (weak, nonatomic) IBOutlet UILabel *month;
@property (weak, nonatomic) IBOutlet UILabel *year;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *discripPoints;


@property (assign, nonatomic) NSUInteger numOfRow;
@end
