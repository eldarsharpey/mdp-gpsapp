//
//  FullStoryViewController.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 20.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import "FullStoryViewController.h"
#import "AppDelegate.h"
#import "DataToday+CoreDataProperties.h"
#import "PlacesToday+CoreDataProperties.h"

@interface FullStoryViewController ()

@end

@implementation FullStoryViewController


NSArray* todayArray2;
NSDictionary* markDictionary2;
NSDictionary* pointsDictionary2;
NSUInteger rowNum2;

NSDictionary* pointsRangeImg2;
NSDictionary* pointsRangeText2;


-(void)viewWillAppear:(BOOL)animated{
        todayArray2 = [(AppDelegate*)[[UIApplication sharedApplication] delegate] TodayData];
        [_todayTableView reloadData];
        
        pointsRangeImg2 = @{@"0" :@"😩", @"1" :@"😔", @"2":@"🤓", @"3":@"😎", @"4":@"🤪",@"5":@"🤩"};
        pointsRangeText2 = @{@"0" :@"Нет активности", @"1" :@"Пошевелил животиком", @"2":@"Помог маме с пакетами", @"3":@"Почистил 10кг картошки", @"4":@"Поднял 100кг штангу",@"5":@"Активнее тебя только фтор!"};
        
        
        
        DataToday* todayDataObj = [todayArray2 objectAtIndex:_numOfRow];
        _date.text =[todayDataObj.date substringWithRange:NSMakeRange(0, 2)];
        _month.text =[todayDataObj.date substringWithRange:NSMakeRange(3, 4)];
        _year.text = [todayDataObj.date substringWithRange:NSMakeRange(9, 5)];
        _imgPoints.text = [pointsRangeImg2 valueForKey: [NSString stringWithFormat:@"%d",todayDataObj.points]];
        _discripPoints.text = [pointsRangeText2 valueForKey: [NSString stringWithFormat:@"%d",todayDataObj.points]];
}




- (void)viewDidLoad {
    [super viewDidLoad];
    
    markDictionary2 = @{@"Нет":@"👀", @"Дом":@"🏰", @"Спорт":@"🏋️‍♀️", @"Учеба":@"🤯", @"Работа":@"🏦"};
    pointsDictionary2 = @{@"Нет":@"0️⃣", @"Дом":@"1️⃣", @"Спорт":@"4️⃣", @"Учеба":@"3️⃣", @"Работа":@"2️⃣"};
 
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    DataToday* todayDataObj = [todayArray2 lastObject];
    NSInteger count = [[todayDataObj placesToday] count];
    return count;
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *c;
    
    c = [tableView dequeueReusableCellWithIdentifier:@"TodayCell"];
    
    UILabel *place;
    UILabel *mark;
    UILabel *img;
    UILabel *points;
    NSInteger index = [indexPath row];
    
    
    DataToday* todayDataObj = [todayArray2 objectAtIndex:_numOfRow];
    
    PlacesToday* placesTodayObj = [[todayDataObj placesToday] objectAtIndex:index];
    
    place = (UILabel*)[c.contentView viewWithTag:1001];
    mark = (UILabel*)[c.contentView viewWithTag:1002];
    img = (UILabel*)[c.contentView viewWithTag:1003];
    points = (UILabel*)[c.contentView viewWithTag:1004];
    
    place.text = placesTodayObj.place;
    NSString * markstring =@"Метка: ";
    mark.text = [markstring stringByAppendingString:placesTodayObj.mark];
    img.text = [markDictionary2  valueForKey:placesTodayObj.mark];
    points.text = [pointsDictionary2 valueForKey: placesTodayObj.mark];
    return c;
    
}


- (IBAction)BackButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
  
}


@end
