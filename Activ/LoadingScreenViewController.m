//
//  LoadingScreenViewController.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 19.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import "LoadingScreenViewController.h"
#import "AppDelegate.h"

@interface LoadingScreenViewController ()

@end

@implementation LoadingScreenViewController

NSProgress* progress;
NSInteger tick;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _pBar.progress = 0;
    [progress respondsToSelector:@selector(onTick)];
    
   [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                  target: self
                                                selector:@selector(onTick)
                                                userInfo: nil repeats:YES];
    
    // Do any additional setup after loading the view.
}

-(void)onTick{
    tick=tick+1;
    float lvl = (float)tick/5;
        [_pBar setProgress: lvl animated:YES];
    if(tick == 2){
        _label.text = @"Загружаю данные GPS...";
    }
    
    if(tick == 4){
        _label.text = @"Загружаю CoreData...";
    }
    
    if(tick == 5 ){
        if ([(AppDelegate*)[[UIApplication sharedApplication] delegate] GPSloaded]) {
            _label.text = @"Успешно";
            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            _label.text = @"Нет соединения с интернетом...";
        }
         
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
