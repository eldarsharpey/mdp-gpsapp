//
//  SelectMarkViewController.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 18.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import "SelectMarkViewController.h"
#import "AlPlaceViewController.h"
#import "Places+CoreDataProperties.h"
#import "AppDelegate.h"

@interface SelectMarkViewController ()

@end

@implementation SelectMarkViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger count = [_markDictionary count];
    return count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *c;
    
    c = [tableView dequeueReusableCellWithIdentifier:@"MarkCell"];
    
    UILabel *mark;
    UILabel *img;
    
     NSInteger index = [indexPath row];
    
    
    mark = (UILabel*)[c.contentView viewWithTag:1001];
    img = (UILabel*)[c.contentView viewWithTag:1002];
    
    mark.text = [[_markDictionary allKeys] objectAtIndex:index];
    img.text =  [[_markDictionary allValues] objectAtIndex:index];
    
    return c;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Places* place = _placesArray[_rowNum];
    place.mark = [[_markDictionary allKeys] objectAtIndex:indexPath.row];
    [place.managedObjectContext save:nil];
    
    [(AppDelegate*)[[UIApplication sharedApplication] delegate] RefreshTodayPlacesMarks];
    [(AppDelegate*)[[UIApplication sharedApplication] delegate] CountPoints];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
