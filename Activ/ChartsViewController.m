//
//  ChartsViewController.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 20.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import "ChartsViewController.h"
#import "DataToday+CoreDataProperties.h"
#import "AppDelegate.h"

@interface ChartsViewController ()

@end

@implementation ChartsViewController


-(void)viewWillAppear:(BOOL)animated{
    
    JBBarChartView *barChartView = [[JBBarChartView alloc] init];
    barChartView.dataSource = self;
    barChartView.delegate = self;
    barChartView.minimumValue = 0;
    barChartView.maximumValue = 5;
    
    barChartView.backgroundColor = _chartView.backgroundColor;
     barChartView.tintColor = _chartView.tintColor;
    [_chartView addSubview:barChartView];
    
    barChartView.frame =  [_chartView bounds];
    [barChartView reloadDataAnimated:YES];
}


- (NSUInteger)numberOfBarsInBarChartView:(JBBarChartView *)barChartView
{
    return [[(AppDelegate*)[[UIApplication sharedApplication] delegate] getBarData] count];
}

- (CGFloat)barChartView:(JBBarChartView *)barChartView heightForBarViewAtIndex:(NSUInteger)index
{
    NSArray* array =[(AppDelegate*)[[UIApplication sharedApplication] delegate] getBarData];
    DataToday* today = [array objectAtIndex:index];
    return today.points; // height of bar at index
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
