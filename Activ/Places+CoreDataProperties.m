//
//  Places+CoreDataProperties.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 18.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//
//

#import "Places+CoreDataProperties.h"

@implementation Places (CoreDataProperties)

+ (NSFetchRequest<Places *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Places"];
}

@dynamic mark;
@dynamic place;

/*

-(void) setPlace:(NSString *)place{
    [self willChangeValueForKey:@"place"];
    [self setPrimitiveValue:place forKey:@"place"];
    [self didChangeValueForKey:@"place"];
}

-(NSString*)place{
    NSString* string = nil;
    [self willAccessValueForKey:@"place"];
    string = [self primitiveValueForKey:@"place"];
    [self didAccessValueForKey:@"place"];
    return string;
}


-(void) setMark:(NSString *)mark{
    [self willChangeValueForKey:@"mark"];
    [self setPrimitiveValue:mark forKey:@"mark"];
    [self didChangeValueForKey:@"mark"];
}

-(NSString*)mark{
    NSString* string = nil;
    [self willAccessValueForKey:@"mark"];
    string = [self primitiveValueForKey:@"mark"];
    [self didAccessValueForKey:@"mark"];
    return string;
}
 */

@end
