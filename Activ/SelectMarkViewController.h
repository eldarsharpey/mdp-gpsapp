//
//  SelectMarkViewController.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 18.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectMarkViewController : UIViewController  <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSDictionary* markDictionary;
@property (assign, nonatomic)  NSUInteger rowNum;
@property (assign, nonatomic)  NSArray* placesArray;
@end
