//
//  AlPlaceViewController.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 18.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//

#import "AlPlaceViewController.h"
#import "AppDelegate.h"
#import "Places+CoreDataProperties.h"
#import "SelectMarkViewController.h"

@interface AlPlaceViewController ()

@end

@implementation AlPlaceViewController

NSArray* placesArray;

-(void)viewWillAppear:(BOOL)animated{
    [_allPlaceTab reloadData];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    placesArray = [(AppDelegate*)[[UIApplication sharedApplication] delegate] placesArray];
    
    _markDictionary = @{@"Нет":@"👀", @"Дом":@"🏰", @"Спорт":@"🏋️‍♀️", @"Учеба":@"🤯", @"Работа":@"🏦"};
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger count = [placesArray count];
    return count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    UITableViewCell *c;
    
    c = [tableView dequeueReusableCellWithIdentifier:@"PlaceCell"];
    
    UILabel *place;
    UILabel *mark;
    UILabel *img;
    NSInteger index = [indexPath row];
    

    Places* placeObj = placesArray[index];
    
    place = (UILabel*)[c.contentView viewWithTag:1001];
    mark = (UILabel*)[c.contentView viewWithTag:1002];
    img = (UILabel*)[c.contentView viewWithTag:1003];
    
    place.text = placeObj.place;
    NSString * markstring =@"Метка: ";
    mark.text = [markstring stringByAppendingString:placeObj.mark];
    
    img.text = [_markDictionary valueForKey:placeObj.mark];
    
    return c;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _rowNum = indexPath.row;
    
    [self performSegueWithIdentifier:@"markSegue" sender:self];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"markSegue"]){
    SelectMarkViewController* markView = segue.destinationViewController;
    markView.markDictionary = _markDictionary;
    markView.rowNum = _rowNum;
    markView.placesArray = placesArray;
     
    }
 
}

@end
