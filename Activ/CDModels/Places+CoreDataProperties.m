//
//  Places+CoreDataProperties.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 18.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//
//

#import "Places+CoreDataProperties.h"

@implementation Places (CoreDataProperties)

+ (NSFetchRequest<Places *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Places"];
}

@dynamic mark;
@dynamic place;

@end
