//
//  DataToday+CoreDataProperties.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 19.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//
//

#import "DataToday+CoreDataProperties.h"

@implementation DataToday (CoreDataProperties)

+ (NSFetchRequest<DataToday *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"DataToday"];
}

@dynamic date;
@dynamic points;
@dynamic steps;
@dynamic placesToday;

@end
