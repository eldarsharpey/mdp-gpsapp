//
//  PlacesToday+CoreDataProperties.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 19.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//
//

#import "PlacesToday+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface PlacesToday (CoreDataProperties)

+ (NSFetchRequest<PlacesToday *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *mark;
@property (nullable, nonatomic, copy) NSString *place;
@property (nonatomic) int32_t points;
@property (nullable, nonatomic, retain) DataToday *dataToday;

@end

NS_ASSUME_NONNULL_END
