//
//  DataToday+CoreDataProperties.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 19.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//
//

#import "DataToday+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DataToday (CoreDataProperties)

+ (NSFetchRequest<DataToday *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *date;
@property (nonatomic) int32_t points;
@property (nonatomic) int32_t steps;
@property (nullable, nonatomic, retain) NSOrderedSet<PlacesToday *> *placesToday;

@end

@interface DataToday (CoreDataGeneratedAccessors)

- (void)insertObject:(PlacesToday *)value inPlacesTodayAtIndex:(NSUInteger)idx;
- (void)removeObjectFromPlacesTodayAtIndex:(NSUInteger)idx;
- (void)insertPlacesToday:(NSArray<PlacesToday *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removePlacesTodayAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInPlacesTodayAtIndex:(NSUInteger)idx withObject:(PlacesToday *)value;
- (void)replacePlacesTodayAtIndexes:(NSIndexSet *)indexes withPlacesToday:(NSArray<PlacesToday *> *)values;
- (void)addPlacesTodayObject:(PlacesToday *)value;
- (void)removePlacesTodayObject:(PlacesToday *)value;
- (void)addPlacesToday:(NSOrderedSet<PlacesToday *> *)values;
- (void)removePlacesToday:(NSOrderedSet<PlacesToday *> *)values;

@end

NS_ASSUME_NONNULL_END
