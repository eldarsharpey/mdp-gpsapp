//
//  PlacesToday+CoreDataClass.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 19.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "Parent+CoreDataClass.h"

@class DataToday;

NS_ASSUME_NONNULL_BEGIN

@interface PlacesToday : Parent

@end

NS_ASSUME_NONNULL_END

#import "PlacesToday+CoreDataProperties.h"
