//
//  PlacesToday+CoreDataProperties.m
//  Activ
//
//  Created by Эльдар Шаяхметов on 19.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//
//

#import "PlacesToday+CoreDataProperties.h"

@implementation PlacesToday (CoreDataProperties)

+ (NSFetchRequest<PlacesToday *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"PlacesToday"];
}

@dynamic mark;
@dynamic place;
@dynamic points;
@dynamic dataToday;

@end
