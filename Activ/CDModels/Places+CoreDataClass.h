//
//  Places+CoreDataClass.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 18.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Places : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Places+CoreDataProperties.h"
