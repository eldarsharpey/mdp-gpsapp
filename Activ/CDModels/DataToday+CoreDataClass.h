//
//  DataToday+CoreDataClass.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 19.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "Parent+CoreDataClass.h"

@class PlacesToday;

NS_ASSUME_NONNULL_BEGIN

@interface DataToday : Parent

@end

NS_ASSUME_NONNULL_END

#import "DataToday+CoreDataProperties.h"
