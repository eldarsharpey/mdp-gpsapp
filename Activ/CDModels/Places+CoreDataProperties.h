//
//  Places+CoreDataProperties.h
//  Activ
//
//  Created by Эльдар Шаяхметов on 18.03.2018.
//  Copyright © 2018 Эльдар Шаяхметов. All rights reserved.
//
//

#import "Places+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Places (CoreDataProperties)

+ (NSFetchRequest<Places *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *mark;
@property (nullable, nonatomic, copy) NSString *place;

@end

NS_ASSUME_NONNULL_END
